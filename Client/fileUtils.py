class File:
    """A simple utility class to manage file paths and contents.
    """

    # The default buffer size to read files from the filesystem with.
    DEFAULT_BUFFER_SIZE = 1024 * 1024

    def __init__(self, absolute_path: str, relative_path: str, buffer_size: int = DEFAULT_BUFFER_SIZE):
        """Creates a file object referencing the provided paths.

        Arguments:
            absolute_path {str} -- The absolute path to the file.
            relative_path {str} -- The path relative to the watched directory of the file.

        Keyword Arguments:
            buffer_size {int} -- The buffer size to read files from the filesystem with (default: {DEFAULT_BUFFER_SIZE})
        """
        self.m_absolute_path = absolute_path
        self.m_relative_path = relative_path
        self.m_buffer_size = File.DEFAULT_BUFFER_SIZE

    def as_bytes(self) -> bytes:
        """Reads the contents of the file into a bytes object.

        Returns:
            bytes -- The bytes contained in the file.
        """
        # Opens a handle to the file in read, binary mode
        file_handle = open(self.m_absolute_path, 'rb')
        data = bytearray()

        # Reads the contents of the file.
        data_chunk = file_handle.read(self.m_buffer_size)
        data.extend(data_chunk)
        while data_chunk:
            data_chunk = file_handle.read(self.m_buffer_size)
            data.extend(data_chunk)

        return bytes(data)
