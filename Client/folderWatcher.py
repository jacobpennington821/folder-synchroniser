import os
import time

from watchdog.events import FileSystemEventHandler, FileSystemEvent
from watchdog.observers import Observer

from fileUtils import File


class FolderWatcher:
    """A class for monitoring changes to a folder and notifying subscribers.
    """

    def __init__(self, directoryPath: str):
        """Creates a new watcher that will monitor the provided directory.

        Arguments:
            directoryPath {str} -- A path to the directory to monitor.

        Raises:
            ValueError: Raised if the provided directory does not exist or is not a directory.
        """
        if not os.path.exists(directoryPath):
            raise ValueError("The provided path doesn't exist on this system.")
        if not os.path.isdir(directoryPath):
            raise ValueError("The provided path is not a directory.")

        self.m_directoryPath = os.path.abspath(directoryPath)
        self.m_eventHandler = FolderWatcher.FolderEventHandler(
            self.m_directoryPath)

    def start_watching(self):
        """Starts watching the directory.
        """
        # Creates a watchdog instance to monitor the directory
        observer = Observer()
        observer.schedule(self.m_eventHandler,
                          self.m_directoryPath, recursive=True)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()

    def subscribe(self, subscriber: object):
        """Adds the provided object to the list of subscribers to be notified of events.

        Arguments:
            subscriber {object} -- An object to add to the subscribers.
        """
        self.m_eventHandler.subscribe(subscriber)

    def unsubscribe(self, subscriber: object):
        """Removes the provided object from the list of subscribers.

        Arguments:
            subscriber {object} -- The object to remove from the subscribers.
        """
        self.m_eventHandler.unsubscribe(subscriber)

    class FolderEventHandler(FileSystemEventHandler):
        """An implementation of the watchdog event handler to monitor changes in the directory.
        """

        def __init__(self, path: str):
            """Creates a new handler with an empty set of subscribers.

            Arguments:
                path {str} -- The path to monitor.
            """
            super().__init__()
            self.m_subscribers = set()
            self.m_directoryPath = path

        def on_moved(self, event: FileSystemEvent):
            """Notifies subscribers of items being renamed in the directory.

            Arguments:
                event {FileSystemEvent} -- The event object, as created by watchdog. Must contain both src_path and dest_path.
            """
            src = File(event.src_path, self._getRelativePath(event.src_path))
            dest = File(event.dest_path,
                        self._getRelativePath(event.dest_path))
            if event.is_directory:
                print("Directory " + str(src.m_relative_path) +
                      " moved to " + str(dest.m_relative_path))
            else:
                print("File " + str(src.m_relative_path) +
                      " moved to " + str(dest.m_relative_path))
            for subscriber in self.m_subscribers:
                subscriber.on_moved(src, dest, event.is_directory)

        def on_created(self, event: FileSystemEvent):
            """Notifies subscribers of items being created in the directory.

            Arguments:
                event {FileSystemEvent} -- The event object, as created by watchdog.
            """
            src = File(event.src_path, self._getRelativePath(event.src_path))
            if event.is_directory:
                print("Directory " + str(src.m_relative_path) + " created")
            else:
                print("File " + str(src.m_relative_path) + " created")
            for subscriber in self.m_subscribers:
                subscriber.on_created(src, event.is_directory)

        def on_deleted(self, event: FileSystemEvent):
            """Notifies subscribers of items being deleted from the directory.

            Arguments:
                event {FileSystemEvent} -- The event object, as created by watchdog.
            """
            src = File(event.src_path, self._getRelativePath(event.src_path))
            if event.is_directory:
                print("Directory " + str(src.m_relative_path) + " deleted")
            else:
                print("File " + str(src.m_relative_path) + " deleted")
            for subscriber in self.m_subscribers:
                subscriber.on_deleted(src, event.is_directory)

        def on_modified(self, event: FileSystemEvent):
            """Notifies subscribers of items' contents being changed in the directory.

            Arguments:
                event {FileSystemEvent} -- The event object, as created by watchdog.
            """
            src = File(event.src_path, self._getRelativePath(event.src_path))
            if event.is_directory:
                print("Directory " + str(src.m_relative_path) + " modified")
            else:
                print("File " + str(src.m_relative_path) + " modified")
            for subscriber in self.m_subscribers:
                subscriber.on_modified(src, event.is_directory)

        def _getRelativePath(self, absolutePath: str) -> str:
            """Gets a path relative to the watched directory from the provided path.

            Arguments:
                absolutePath {str} -- The path to convert to a relative path.

            Returns:
                str -- A path relative to the watched directory.
            """
            return os.path.relpath(absolutePath, self.m_directoryPath)

        def subscribe(self, subscriber: object):
            """Adds the provided object to the set of subscribers to notify of events.

            Arguments:
                subscriber {object} -- The subscriber to add.
            """
            self.m_subscribers.add(subscriber)

        def unsubscribe(self, subscriber: object):
            """Removes the provided object from the set of subscribers to notify.

            Arguments:
                subscriber {object} -- The subscriber to remove.
            """
            self.m_subscribers.remove(subscriber)
