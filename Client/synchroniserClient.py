import sys
from folderWatcher import FolderWatcher
from updateSender import UpdateSender


def main(args: list):
    """Main method to start the synchroniser client.

    Arguments:
        args {list} -- The arguments to supply to the program. The first argument must be a path to the directory to synchronise. The second argument may be an IPv4 address of the server to connect to.
    """
    if len(args) < 2:
        print("Please supply a directory name to use!")
        return
    if len(args) == 2:
        # Takes directory argument
        update_sender = UpdateSender()
    elif len(args) == 3:
        # Takes directory and address arguments
        address = args[2]
        update_sender = UpdateSender(address=address)
    elif len(args) == 4:
        # Takes directory, address and port arguments
        try:
            address = args[2]
            port = int(args[3])
            update_sender = UpdateSender(address=address, port=port)
        except ValueError:
            print("Invalid port")
            return
    else:
        print("Unrecognised number of arguments")
        return
    directory_path = args[1]
    watcher = FolderWatcher(directory_path)
    watcher.subscribe(update_sender)
    print("Started synchroniser client")
    try:
        watcher.start_watching()
    except KeyboardInterrupt:
        print("Shutting down.")


if __name__ == "__main__":
    main(sys.argv)
