import socket
import time

from fileUtils import File


class UpdateSender:
    """A class to send folder updates over TCP to a server.
    """

    DEFAULT_PORT = 51255
    DEFAULT_ADDRESS = "127.0.0.1"

    def __init__(self, address: str = DEFAULT_ADDRESS, port: int = DEFAULT_PORT):
        """Creates an update sender which will send updates to the provided address and port.

        Keyword Arguments:
            address {str} -- The IPv4 address of the server to send updates to. (default: {DEFAULT_ADDRESS})
            port {int} -- The port number to send updates to. (default: {DEFAULT_PORT})
        """
        self.m_sock = None
        self.m_address = address
        self.m_port = port

    def __del__(self):
        """Destroys the object, making sure to disconnect the socket.
        """
        self._disconnect()

    def on_moved(self, src: File, dest: File, is_directory: bool):
        """The function called when a folder or file is renamed.

        Arguments:
            src {File} -- The File object of where the item was moved from.
            dest {File} -- The File object of where the item was moved to.
            is_directory {bool} -- Whether the item is a directory or a file.
        """
        message = UpdateSender.Message(
            "MOV", is_directory, src.m_relative_path, dest.m_relative_path)
        try:
            self._connect()
            self._send(message)
            self._disconnect()
        except ConnectionError as e:
            print(e)

    def on_created(self, src: File, is_directory: bool):
        """The function called when a folder or file is created.

        Arguments:
            src {File} -- The File object of where the item was created.
            is_directory {bool} -- Whether the item is a directory or a file.
        """
        message = UpdateSender.Message(
            "NEW", is_directory, src.m_relative_path)
        try:
            self._connect()
            self._send(message)
            self._disconnect()
        except ConnectionError as e:
            print(e)

    def on_deleted(self, src: File, is_directory: bool):
        """The function called when a folder or file is deleted.

        Arguments:
            src {File} -- The File object of where the item was deleted from.
            is_directory {bool} -- Whether the item was a directory or a file.
        """
        message = UpdateSender.Message(
            "DEL", is_directory, src.m_relative_path)
        try:
            self._connect()
            self._send(message)
            self._disconnect()
        except ConnectionError as e:
            print(e)

    def on_modified(self, src: File, is_directory: bool):
        """The function called when a folder or file is changed.

        Arguments:
            src {File} -- The File object referencing the item that was changed.
            is_directory {bool} -- Whether the item is a directory or a file.
        """
        try:
            # Gets the data from the file
            fileData = src.as_bytes()
            message = UpdateSender.Message(
                "MOD", is_directory, src.m_relative_path, payload=fileData)
            self._connect()
            self._send(message)
            self._disconnect()
        except ConnectionError as e:
            print(e)
        except Exception:
            print("Failed to open file, skipping update.")

    def _connect(self, max_retries=3):
        """Connects to the server and establishes a TCP connection.

        Keyword Arguments:
            max_retries {int} -- The maximum number of connection attempts before failing. (default: {3})

        Raises:
            ConnectionError: Raised when the socket fails to connect after the given number of retries.
        """
        self.m_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        connected = False
        retries = 0

        while not connected and retries < max_retries:
            try:
                retries += 1
                # print("(re)connecting to server at " + str(self.m_address) + ":" + str(self.m_port))
                self.m_sock.connect((self.m_address, self.m_port))
                connected = True
            except socket.error:
                time.sleep(2)

        if not connected:
            raise ConnectionError("Error connecting to the server at " +
                                  str(self.m_address) + ":" + str(self.m_port))

    def _disconnect(self):
        """Ends the TCP connection.
        """
        if self.m_sock:
            self.m_sock.close()

    class Message:
        """A class for easily creating event messages to send to the server.
        """

        SEPARATOR = b"|"
        END_OF_HEADER = b"END_OF_HEADER"

        def __init__(self, command: str, is_directory: bool, src_path: str, dest_path: str = None, payload: bytes = None):
            """Creates a message with the provided values.

            Arguments:
                command {str} -- The 3 letter command to send, one of MOV (moved), NEW (created), DEL (delete), MOD (modified)
                is_directory {bool} -- Whether the event target is a file or directory.
                src_path {str} -- The relative path, from the folder being watched, to the source of the event.

            Keyword Arguments:
                dest_path {str} -- The relative path, from the folder being watched, to the destination of the event, if present. (default: {None})
                payload {bytes} -- The payload of the message, if present. (default: {None})
            """
            self.m_command = command
            self.m_is_directory = is_directory
            self.m_src_path = src_path
            self.m_dest_path = dest_path
            self.m_payload = payload

        def as_bytes(self) -> bytes:
            """Gets the message as a bytes object ready to send to the server.

            Returns:
                bytes -- A representation of the message in a bytes object.
            """
            array = bytearray()
            array.extend(self.m_command.encode())
            array.extend(UpdateSender.Message.SEPARATOR)
            if self.m_is_directory:
                array.extend(b"1")
            else:
                array.extend(b"0")
            array.extend(UpdateSender.Message.SEPARATOR)
            array.extend(self.m_src_path.encode())
            if self.m_dest_path:
                array.extend(UpdateSender.Message.SEPARATOR)
                array.extend(self.m_dest_path.encode())

            array.extend(UpdateSender.Message.END_OF_HEADER)

            if self.m_payload:
                array.extend(self.m_payload)

            return bytes(array)

    def _send(self, message: Message):
        """Sends the provided message over the socket.

        Arguments:
            message {Message} -- The message to send.
        """
        self.m_sock.sendall(message.as_bytes())
