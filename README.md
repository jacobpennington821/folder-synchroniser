# Folder Synchroniser
A simple program using [watchdog](https://pypi.org/project/watchdog/) to keep changes in one folder synchronised with another. Tested on Windows.
## Running
Folder Synchroniser requires Python 3.7+ to run.
To install dependencies do ``pip install -r requirements.txt``
### Server
To run the server simply do ``python <pathtosynchroniser>\Server\synchroniserServer.py <pathtoemptydir> <port>``. ``<port>`` is optional and defaults to 51255.
### Client
To run the client simply do ``python <pathtosynchroniser>\Client\synchroniserClient.py <pathtodirtowatch> <addressofserver> <portofserver>``. ``<addressofserver>`` and ``<portofserver>`` are optional and default to 127.0.0.1 and 51255 respectively.

