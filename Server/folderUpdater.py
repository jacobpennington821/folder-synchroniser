import os
import shutil
import stat


class FolderUpdater:
    """A class for updating the contents of a folder based on received updates.
    """

    def __init__(self, directory_path: str):
        """Creates a new updater with the provided directory.

        Arguments:
            directory_path {str} -- The empty directory to fill with any updates received.

        Raises:
            ValueError: Raised if the provided directory does not exist, is not a directory or isn't empty.
        """
        if not os.path.exists(directory_path):
            raise ValueError("The provided path doesn't exist on this system.")
        if not os.path.isdir(directory_path):
            raise ValueError("The provided path is not a directory.")
        if os.listdir(directory_path):
            raise ValueError("The provided folder isn't empty!")
        self.m_directory_path = directory_path

    def move_file(self, src_file_path: str, dest_file_path: str):
        """Moves the file located at the src_file_path to the location specified in dest_file_path.

        Arguments:
            src_file_path {str} -- The relative path of the file to move.
            dest_file_path {str} -- The relative path of the destination of the file.
        """
        src_abs_path = self._get_absolute_path(src_file_path)
        dest_abs_path = self._get_absolute_path(dest_file_path)

        if os.path.exists(src_abs_path) and not os.path.exists(dest_abs_path):
            try:
                os.rename(src_abs_path, dest_abs_path)
            except Exception:
                print("Failed to rename " + src_file_path +
                      " to " + dest_file_path)

    def move_folder(self, src_folder_path: str, dest_folder_path: str):
        """Moves the folder located at the src_folder_path to the location specified in dest_folder_path.

        Arguments:
            src_folder_path {str} -- The relative path of the folder to move.
            dest_folder_path {str} -- The relative path of the destination of the folder.
        """
        src_abs_path = self._get_absolute_path(src_folder_path)
        dest_abs_path = self._get_absolute_path(dest_folder_path)

        if os.path.exists(src_abs_path) and not os.path.exists(dest_abs_path):
            try:
                os.rename(src_abs_path, dest_abs_path)
            except Exception:
                print("Failed to rename " + src_folder_path +
                      " to " + dest_folder_path)

    def create_file(self, file_path: str):
        """Creates a file at the provided path.

        Arguments:
            file_path {str} -- The relative path of the file to create.
        """
        try:
            file_handle = open(self._get_absolute_path(file_path), "wb")
            file_handle.close()
        except Exception:
            print("Failed to create " + file_path)

    def create_folder(self, folder_path: str):
        """Creates a folder at the provided path.

        Arguments:
            folder_path {str} -- The relative path of the folder to create.
        """
        abs_path = self._get_absolute_path(folder_path)
        if not os.path.exists(abs_path):
            try:
                os.mkdir(abs_path)
            except Exception:
                print("Failed to make " + folder_path)

    def delete_file(self, file_path: str):
        """Deletes the file located at the provided path.

        Arguments:
            file_path {str} -- The relative path of the file to delete.
        """
        abs_path = self._get_absolute_path(file_path)

        # Fix for watchdog getting confused between folders and files
        if os.path.isdir(abs_path):
            self.delete_folder(file_path)

        elif os.path.exists(abs_path):
            try:
                os.remove(abs_path)
            except Exception:
                print("Failed to delete " + file_path)

    def delete_folder(self, folder_path: str):
        """Deletes the folder located at the provided path.

        Arguments:
            folder_path {str} -- The relative path of the folder to delete.
        """
        abs_path = self._get_absolute_path(folder_path)
        if os.path.exists(abs_path) and os.path.isdir(abs_path):
            try:
                # Deletes the folder the long way instead of shutil due to permission issues
                for root, dirs, files in os.walk(abs_path, topdown=False):
                    for name in files:
                        filename = os.path.join(root, name)
                        # Tries to get write permissions for the file
                        os.chmod(filename, stat.S_IWUSR)
                        os.remove(filename)
                    for name in dirs:
                        os.rmdir(os.path.join(root, name))
                os.rmdir(abs_path)
            except Exception:
                print("Failed to delete " + folder_path)

    def modify_file(self, file_path: str, data: bytes):
        """Modifies the contents of the file at the provided path with the provided data.

        Arguments:
            file_path {str} -- The relative path of the file to change.
            data {bytes} -- The data to fill the file with.
        """
        abs_path = self._get_absolute_path(file_path)
        try:
            file_handle = open(abs_path, "wb")
            file_handle.write(data)
            file_handle.close()
        except Exception:
            print("Failed to modify " + file_path)

    def modify_folder(self, folder_path: str, data: bytes):
        """Modifies the contents of the folder at the provided path.

        Arguments:
            folder_path {str} -- The folder to modify
            data {bytes} -- The data to fill the folder with.
        """
        # Current unused, modify file does the job.
        pass

    def _get_absolute_path(self, relative_path: str) -> str:
        """Converts the provided path relative to the updated directory, to an absolute path.

        Arguments:
            relative_path {str} -- A path relative to the directory being used to store updates.

        Returns:
            str -- An absolute path to the provided path.
        """
        return os.path.join(self.m_directory_path, relative_path)
