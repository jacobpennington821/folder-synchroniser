import sys
from folderUpdater import FolderUpdater
from updateReceiver import UpdateReceiver


def main(args: list):
    """Main method to start the synchroniser server.

    Arguments:
        args {list} -- The arguments to supply to the program. The first argument must be a path to an empty directory to store all received changes. The second, optional, argument can be the port number to run on.
    """
    if len(args) < 2:
        print("Please supply a directory to use!")
        return
    if len(args) == 2:
        receiver = UpdateReceiver()
    elif len(args) == 3:
        try:
            port = int(args[2])
            receiver = UpdateReceiver(port=port)
        except ValueError:
            print("Invalid port")
            return
    directoryPath = args[1]
    updater = FolderUpdater(directoryPath)
    receiver.subscribe(updater)
    print("Started synchroniser server")
    receiver.listen()


if __name__ == "__main__":
    main(sys.argv)
