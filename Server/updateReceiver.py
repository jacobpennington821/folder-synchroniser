import socket


class UpdateReceiver:
    """A class to receive updates from a client and notify subscribers.
    """

    DEFAULT_PORT = 51255
    DEFAULT_RECEIVE_BUFFER_SIZE = 1024 * 1024

    SEPARATOR = "|"
    END_OF_HEADER = b"END_OF_HEADER"

    def __init__(self, port: int = DEFAULT_PORT, receive_buffer_size: int = DEFAULT_RECEIVE_BUFFER_SIZE):
        """Creates a receiver to handle incoming updates.

        Keyword Arguments:
            port {int} -- The port to listen on. (default: {DEFAULT_PORT})
            receive_buffer_size {int} -- The size of the receive buffer. (default: {DEFAULT_RECEIVE_BUFFER_SIZE})
        """
        self.m_sock = None
        self.m_subscribers = set()
        self.m_receive_buffer_size = receive_buffer_size
        self.m_port = port

    def listen(self):
        """Begins listening for updates from a client, notifying subscriber of any updates.
        """
        self.m_sock = socket.socket()
        self.m_sock.bind(('', self.m_port))
        self.m_sock.settimeout(2)

        try:
            self.m_sock.listen()
            while True:
                try:
                    conn, address = self.m_sock.accept()
                    data = conn.recv(self.m_receive_buffer_size)
                    packet = data
                    while data:
                        data = conn.recv(self.m_receive_buffer_size)
                        packet += data
                    conn.shutdown(socket.SHUT_RDWR)
                    conn.close()

                    # Splits the packet into the header and the payload
                    packet_parts = packet.split(
                        UpdateReceiver.END_OF_HEADER, 1)
                    if len(packet_parts) == 0:
                        print("Invalid packet received")
                        continue
                    elif len(packet_parts) == 1:
                        self.handle_update(packet_parts[0].decode())
                    else:
                        self.handle_update(
                            packet_parts[0].decode(), packet_parts[1])
                except socket.timeout:
                    pass
                except Exception as e:
                    print(e)
                    conn.close()
        except KeyboardInterrupt:
            print("Server shutting down.")
            self.m_sock.close()

    def subscribe(self, subscriber: object):
        """Adds the object to the set of subscribers to notify of updates.

        Arguments:
            subscriber {object} -- An object to add to the subscribers.
        """
        self.m_subscribers.add(subscriber)

    def unsubscribe(self, subscriber: object):
        """Removes the object from the set of subscribers.

        Arguments:
            subscriber {object} -- The object to remove from the subscribers
        """
        self.m_subscribers.remove(subscriber)

    def handle_update(self, packet_header: str, packet: bytes = None):
        """Handles the provided packet and notifies subscribers of the update appropriately.

        Arguments:
            packet_header {str} -- The header of the packet containing command, is_directory, src and [dest]

        Keyword Arguments:
            packet {bytes} -- The packet payload. (default: {None})
        """
        packet_header_parts = packet_header.split(UpdateReceiver.SEPARATOR)
        if len(packet_header_parts) < 3:
            print("Invalid packet received, dropping")
            return

        command = packet_header_parts[0]
        is_directory = packet_header_parts[1]
        src_path = packet_header_parts[2]

        for subscriber in self.m_subscribers:
            if command == "MOV":
                dest_path = packet_header_parts[3]
                if is_directory == "0":
                    subscriber.move_file(src_path, dest_path)
                elif is_directory == "1":
                    subscriber.move_folder(src_path, dest_path)
            elif command == "NEW":
                if is_directory == "0":
                    subscriber.create_file(src_path)
                elif is_directory == "1":
                    subscriber.create_folder(src_path)
            elif command == "DEL":
                if is_directory == "0":
                    subscriber.delete_file(src_path)
                elif is_directory == "1":
                    subscriber.delete_folder(src_path)
            elif command == "MOD":
                if is_directory == "0":
                    subscriber.modify_file(src_path, packet)
                elif is_directory == "1":
                    subscriber.modify_folder(src_path, packet)
